# Descrição do teste

Criar uma API Rest para gerenciamento de clubes e associados em esquema de assinatura.

# Requisitos de desenvolvimento

- A API deverá ser feita utilizando o PHP com o framework de desenvolvimento Laravel. 

- Deverá ser utilizado um banco de dados relacional, preferencialmente Open Source (PostgreSQL, MySQL Community ou MariaDB)

- As respostas da API deverão ser enviadas em JSON.

# Requisitos funcionais

- A aplicação deve permitir o cadastro, listagem, atualização e exclusão de clubes (preferencialmente com soft deletes).

- A aplicação deve permitir o cadastro, listagem, atualização e exclusão de usuários (preferencialmente com soft deletes).

- A aplicação deve permitir a associação entre usuários e clubes, sendo que cada usuário pode estar relacionado a N clubes.

- Ao deletar um clube, a aplicação não deverá deletar os usuários relacionados a ele, apenas a relação.

- Ao relacionar um usuário com um clube, a aplicação deve criar para este usuário uma fatura fictícia por mês, representando a assinatura. **(Dica: utilizar scheduled tasks do Laravel para isso)**

- As faturas devem possuir um campo para representar o status de pagamento.

- [Opcional]: as faturas devem ter um campo para representar a data de vencimento. Será considerada vencida uma fatura não paga após a sua data de vencimento.

- A aplicação deve possuir uma rota para "pagar" uma fatura, alterando o status da mesma no sistema.

- A aplicação deverá possuir uma rota que retorne o status atual de um usuário em relação aos seus clubes (possíveis status abaixo).

| Status       | Condição                                                                                                                           |
| ------------ | ---------------------------------------------------------------------------------------------------------------------------------- |
| Ativo        | O usuário possui faturas com o clube, todas pagas                                                                                  |
| Inadimplente | O usuário possui faturas com o clube, entretanto alguma não está paga (ou vencida, caso opte por implementar a data de vencimento) |
| Inativo      | O usuário não possui faturas com o clube                                                                                           |


# Considerações finais

1. A entrega do teste deve ser feita preferencialmente por um repositório Git público.

2. É preferível que se use os recursos disponíveis do Laravel sempre que possível (Eloquent, Migrations, Router e etc);

3. Para facilitar os testes, também é preferível que seja colocada no projeto uma imagem Docker, caso opte por não fazer isso, insira uma documentação de como executar o seu projeto. ~~E pra quem adicionar uma collection do Postman com as rotas da API, fica a promessa de uma bebida de sua escolha num futuro happy hour!~~

4. Outras coisas que não foram citadas podem te dar "pontos extras", como testes automatizados, caching e documentação. Mas não se sinta pressionado a fazer isso, foque nos requisitos que foram passados acima que já está ótimo!

5. Qualquer dúvida, entre em contato com a gente.